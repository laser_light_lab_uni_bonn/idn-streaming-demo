# IDN-Streaming-Demo

This is a collection of information for participants of the IEEE LCN 2020 Demo (Nov. 18, 2020) and the ILDA 2020 Cloud Conference (starting Nov. 20, 2020) who want to get actively involved in the demo. Please also read the **DISCLAIMER** at the bottom of this page.

## UPDATE March 20, 2021, for the virtual LaserFreak meeting: The necessary OpenVPN profile, updated software of IDN-Toolbox and IDN-Laser-VR and link to YouTube Livestream will be available to LaserFreak participants separately.

## UPDATE Nov. 24, 2020: The IDN streaming experiment has successfully been concluded at the ILDA 2020 Cloud Conference. For the time being, the experiment via a VPN at the University of Bonn is currently no longer running. Follow-up activities will be announced to ILDA members. If you want to place a reservation of your interest in future IDN VPN streaming experiments, please drop an e-mail to laser-light-lab@uni-bonn.de .


## Prerequisites:

- you need a Windows computer (... the IDN visualization software is not (yet) running on Linux, MacOS)
- you need to connect to an OpenVPN server (operated at Uni Bonn)
- you need to have Internet access with preferably > 15 Mbps downlink data rate
- _however, lower downlink data rate may show interesting effects ;-)_


## Step 0: video

If you have not yet done, please watch the demo teaser video, to get an idea of what you could see in the demo:

https://www.youtube.com/watch?v=fh024oQSOyU&t=39s


## Step 1: OpenVPN software

Install OpenVPN software on your computer. We have recently tested the OpenVPN GUI graphical frontend for OpenVPN on Windows:
https://github.com/OpenVPN/openvpn-gui/

Convenient Windows installers are available at:
http://build.openvpn.net/downloads/snapshots/


## Step 2: OpenVPN profile

Download the following OpenVPN profile and use this profile with the OpenVPN software to connect to the IDN VPN server running at Uni Bonn:

File idnvpn-ilda.ovpn is available here **CURRENTLY NOT AVAILABLE**


## Step 3a: IDN visualization software (2D)

Download our IDN visualization software IDN-Toolbox. This is well suited for preview of laser graphic shows. UNZIP on your computer and execute idn-toolbox.exe.

Download link here	https://uni-bonn.sciebo.de/s/NyOy2lMwThRpwXZ

Comprehensive video documentation available at: https://www.youtube.com/watch?v=Jj_wTgCwtMA&t=472s


## IMPORTANT: run *either* IDN-Toolbox *or* IDN-Laser-VR, *but not both* at the same time

Background: The software is opening an UDP socket with port 7255 to reveive IDN stream data. Only one process at a time can do this on the same computer. If you have *two* computers, you can connect both to the IDN VPN and start IDN-Toolbox on one and IDN-Laser-VR on the other computer.

It may happen that you need to confirm the installation of software from unknown sources.

It may happen that you need to confirm Windows firewall settings to accept/allow for network communication in your network.


## Step 3b: IDN visualization software (3D)

Download our IDN visualization software IDN-Laser-VR. This is well suited for preview of laser beam shows with several projectors. UNZIP on your computer and execute LaserVR.exe

The setup in the virtual room is using the typical ILDA conference scenario with 7 beam/aerial laser projectors
and 3 graphic laser projectors.

A good graphics card is recommended for good performance. If you have a VR capable graphics card and a VR headset (and an installation of steam VR), you can see the laser visualization in your VR headset!

Download link here	https://uni-bonn.sciebo.de/s/Bymqym2mNZBAvmn

Comprehensive video documentation available at: https://www.youtube.com/watch?v=mm94htlI5fM&t=1140s


## Step 4: check your visualization output

During the live demo time you can cross-check via a YouTube Livestream that will be provided to the conference participants.

In the days before the conference you can already check on the following YouTube Livestream:

Link here: **CURRENTLY, NO VPN STREAMING !!!**

The last stream recording is still available (5h 26min)
 https://youtu.be/k95dM-9pGPA
 !!! Updated Nov 21, 2020 at 16:52h (pm) CET !!!

**(check for updates frequently, the link may change when a restart is needed)**

This Livestream shows a preview of laser stream data that is being sent to the IDN VPN. The background music is (intentionally)
not matching to the laser display. "Correct" music will play at the conference demo.


## Further remarks:

- Zoom session links for the live demo will be (or have been) provided by the conference organizers for conference participants

- the YouTube Livestream link for the live demo will be provided in the Zoom session of the demo (ILDA will use a separate system)


## Feedback

I would be happy to receive feedback on your experience using our IDN tools on your side. You may send me just comments, or also screenshots, screen capture video, or similar. Please also tell me about the speed of your Internet connection (downlink, uplink).

Just drop an e-mail to laser-light-lab@uni-bonn.de


# DISCLAIMER

Please be aware that the IDN Stream Specification is an open protocol, like e.g. IP, the Internet Protocol, and its documentation is freely available to everybody (download at ILDA Technical Standards, https://www.ilda.com/technical.htm ). "_The IDN-Stream standard describes the encoding of laser show artwork into digital data streams._"

The specification and also manifold hardware and software elements have been implemented by volunteers, who are interested and motivated to foster the development of the open and publicly available components of the ILDA Digital Network. See more at http://ilda-digital.com

Up to now, no effort has been spent into encrypting or authenticating the IDN stream - as maybe known from other media types. 

When using our IDN tools, the copyrights of the artists who have created laser shows or laser show content have to be respected, as well as possible license agreements of the laser show system that is used together with our IDN tools.

**The University of Bonn / the Laser & Light Lab is not liable for damages or copyright/license infringements coming from third person's or third parties' use of the IDN prototype software provided via these gitlab.com groups or project repositories.**
